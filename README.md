# CS_4170_SP2021_Project_SHARMA

Title: Ant Colony Optimization: Literature Survey and Parallelization Methodologies <br>
Name: Devansh Sharma, Graduate Student, Spring'2021.<br>
This project is based upon the Ant Colony Optimization and it's progression through the different versions in order to solve various combinatorial problems. It is effectively very good in tackling any Shortest-path based problem. We will also discuss ways to parallelize it in order to increase efficiency.
